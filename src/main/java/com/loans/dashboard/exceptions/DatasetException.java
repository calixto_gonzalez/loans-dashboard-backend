package com.loans.dashboard.exceptions;

public class DatasetException extends RuntimeException {
    public DatasetException(String message) {
        super(message);
    }
}
