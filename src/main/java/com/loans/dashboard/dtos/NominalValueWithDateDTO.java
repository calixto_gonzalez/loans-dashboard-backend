package com.loans.dashboard.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NominalValueWithDateDTO {
    private String name;
    private Double value;
}
