package com.loans.dashboard.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.loans.dashboard.exceptions.DatasetException;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
public class NominalValueGDPDTO {
    private String name;
    private String description;
    private LocalDateTime refreshedAt;
    private LocalDate newestAvailableDate;
    private LocalDate oldestAvailableDate;
    private String frequency;
    private String type;
    private LocalDate startDate;
    private LocalDate endDate;
    private List<NominalValueWithDateDTO> data;

    @JsonProperty("dataset")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public void fromDataset(Map<String, Object> dataset) {
        this.name = (String) dataset.get("name");
        this.description = (String) dataset.get("description");
        this.refreshedAt = LocalDateTime.parse((String) dataset.get("refreshed_at"), DateTimeFormatter.ISO_ZONED_DATE_TIME);
        this.startDate = LocalDate.parse((String) dataset.get("start_date"), DateTimeFormatter.ISO_LOCAL_DATE);
        this.endDate = LocalDate.parse((String) dataset.get("end_date"), DateTimeFormatter.ISO_LOCAL_DATE);
        this.newestAvailableDate = LocalDate.parse((String) dataset.get("newest_available_date"), DateTimeFormatter.ISO_LOCAL_DATE);
        this.oldestAvailableDate = LocalDate.parse((String) dataset.get("oldest_available_date"), DateTimeFormatter.ISO_LOCAL_DATE);
        this.frequency = (String) dataset.get("frequency");
        this.type = (String) dataset.get("type");
        this.data = ((ArrayList<Object>) dataset.get("data")).stream().map(o -> {
            if (o instanceof ArrayList) {
                ArrayList<Object> actualData = (ArrayList<Object>) o;
                Double value = (Double) actualData.get(1);
                String dateString = (String) actualData.get(0);
                return new NominalValueWithDateDTO(dateString, value);
            } else {
            throw new DatasetException("Wrong type of dataset from source");}

        }).collect(Collectors.toList());


    }
}
