package com.loans.dashboard.dtos;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RegistrationForm {
    @NotNull(message = "Email should be defined")
    @NotEmpty(message = "Email should be defined")
    @Email(message = "Email is not valid")
    private String email;
    @NotNull(message = "Name should be defined")
    @NotEmpty(message = "Name should be defined")
    private String name;
    @NotNull(message = "Last name should be defined")
    @NotEmpty(message = "Last name should be defined")
    private String lastName;
    @NotNull(message = "Password should be defined")
    @NotEmpty(message = "Password should be defined")
    @Size(min = 8, message = "Password should be have more than 7 characters")
    private String password;
}
