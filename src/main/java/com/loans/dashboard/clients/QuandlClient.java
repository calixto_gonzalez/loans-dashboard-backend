package com.loans.dashboard.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loans.dashboard.dtos.NominalValueGDPDTO;
import com.loans.dashboard.exceptions.DatasetException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;

@Service
@Slf4j
public class QuandlClient {

    @Value("${quandl.client.read.timeout}")
    private int readTimeout;
    @Value("${quandl.client.connection.timeout}")
    private int connectionTimeout;
    @Value(value = "${quandl.client.url}")
    private String url;
    private ObjectMapper mapper;

//    private RestTemplate restTemplate;
    private RestTemplate restTemplate = new RestTemplate();

//    @Autowired
//    public QuandlClient(RestTemplateBuilder restTemplateBuilder) {
//        this.mapper = new ObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE).setSerializationInclusion(JsonInclude.Include.NON_NULL);
//        MappingJackson2HttpMessageConverter mappingJackson = new MappingJackson2HttpMessageConverter(this.mapper);
//        mappingJackson.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
//        restTemplate = restTemplateBuilder.messageConverters(Arrays.asList(new ByteArrayHttpMessageConverter(), mappingJackson)).setConnectTimeout(Duration.ofSeconds(connectionTimeout)).setReadTimeout(Duration.ofSeconds(readTimeout)).build();
//    }

    public NominalValueGDPDTO getNominalValueGDP() {
        return this.restTemplate.getForObject(url.concat("/WPSD/NLD_DP_DOD_DLLO_CR_BC_CD.json?api_key=BXvyii-c15W8U1jn5DUV"), NominalValueGDPDTO.class);

    }

    private LinkedHashMap getDataset(Object object) throws DatasetException {
        if (object instanceof LinkedHashMap) {
                return (LinkedHashMap) ((LinkedHashMap) object).get("dataset");
        }
        throw new DatasetException("dataset is not defined");
    }
}
