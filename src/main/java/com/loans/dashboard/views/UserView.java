package com.loans.dashboard.views;

public interface UserView {
    String getEmail();
    String getName();
    String getLastName();
}
