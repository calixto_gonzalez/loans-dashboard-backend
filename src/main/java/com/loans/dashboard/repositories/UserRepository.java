package com.loans.dashboard.repositories;

import com.loans.dashboard.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    User getUserByEmail(String email);
    @Query("SELECT u FROM User u")
    List<User> getAllUsers();
}
