package com.loans.dashboard.controllers;

import com.loans.dashboard.dtos.RegistrationForm;
import com.loans.dashboard.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@Slf4j
@RequestMapping("/register")
public class RegistrationController {

    private final UserService userServiceImpl;

    public RegistrationController(UserService userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    @PostMapping
    public ResponseEntity<Object> createUser(@Valid @RequestBody RegistrationForm user) {
        log.info("Creating user with email: " + user.getEmail());
        this.userServiceImpl.createUser(user);
        return ResponseEntity.ok().build();
    }
}
