package com.loans.dashboard.controllers;

import com.loans.dashboard.services.EconomicInformationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
@AllArgsConstructor
public class EconomicInformationController {
    private final EconomicInformationService economicInformationService;

    @GetMapping("/economic/nominalValueGDP")
    public ResponseEntity<Object> getNominalValueGDP() {
        log.info("Getting nominal Value GDP for the netherlands");
        return ResponseEntity.ok(this.economicInformationService.getNominalValueGDP());
    }
}
