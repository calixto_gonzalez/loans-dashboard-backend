package com.loans.dashboard.controllers;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@RequestMapping("/login")
@Controller
public class LoginController {
    @GetMapping
    @ResponseBody
    public boolean currentUserName(Principal principal) {
        return SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
    }
}
