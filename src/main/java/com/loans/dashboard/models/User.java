package com.loans.dashboard.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "users")
@NoArgsConstructor
public class User {
    @Id
    private String email;
    private String name;
    private String lastName;
    private String password;
    private String role;
}
