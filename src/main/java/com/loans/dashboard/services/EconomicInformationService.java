package com.loans.dashboard.services;

import com.loans.dashboard.dtos.NominalValueGDPDTO;

public interface EconomicInformationService {
    //    NominalValueGDPDTO getNominalValueGDP();
    NominalValueGDPDTO getNominalValueGDP();
}
