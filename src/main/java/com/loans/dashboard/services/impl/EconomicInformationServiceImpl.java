package com.loans.dashboard.services.impl;

import com.loans.dashboard.clients.QuandlClient;
import com.loans.dashboard.dtos.NominalValueGDPDTO;
import com.loans.dashboard.services.EconomicInformationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


@AllArgsConstructor
@Service
public class EconomicInformationServiceImpl implements EconomicInformationService {

    private final QuandlClient quandlClient;

    @Override
    public NominalValueGDPDTO getNominalValueGDP() {
        return quandlClient.getNominalValueGDP();
    };
}
