package com.loans.dashboard.services.impl;

import com.loans.dashboard.dtos.RegistrationForm;
import com.loans.dashboard.models.User;
import com.loans.dashboard.repositories.UserRepository;
import com.loans.dashboard.services.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void createUser(RegistrationForm registrationForm) {
        log.info("Creating user with email: "+registrationForm.getEmail());
        userRepository.save(getUserFromForm(registrationForm));
    }

    private User getUserFromForm(RegistrationForm registrationForm) {
        User user = new User();
        user.setEmail(registrationForm.getEmail());
        user.setName(registrationForm.getName());
        user.setLastName(registrationForm.getLastName());
        user.setRole("ROLE_USER");
        user.setPassword(passwordEncoder.encode(registrationForm.getPassword()));
        return user;
    }
}
