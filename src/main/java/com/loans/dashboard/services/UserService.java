package com.loans.dashboard.services;

import com.loans.dashboard.dtos.RegistrationForm;

public interface UserService {
    void createUser(RegistrationForm user);
}
